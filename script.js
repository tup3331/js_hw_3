let userAge = prompt("Введіть свій вік.");
let alertAge = userAge < 12 ? "Ви є дитиною." : userAge < 18 ? "Ви є підлітком." : "Ви є дорослим.";
alert(alertAge);

// or

// let userAge = prompt("Введіть свій вік.");

// if (isNaN (userAge)) {
//     alert("Введено не число.");
// }   else if (userAge < 12) {
//     alert("Ви є дитиною.");
// }   else if (userAge < 18) {
//     alert("Ви є підлітком.");
// }   else {
//     alert("Ви є дорослим.");
// }

/////////////////////////////////////////////////////

let month = prompt ("Введіть місяць року.");

switch (month.toLowerCase ()) {
    case "січень":
    case "березень":
    case "травень":
    case "липень":
    case "серпень":
    case "жовтень":
    case "грудень":
        console.log("У цьому місяці 31 день.");
        break;

    case "квітень":
    case "червень":
    case "вересень":
    case "листопад":
        console.log("У цьому місяці 30 днів.");
        break;
    
    case "лютий":
        console.log("У цьому місяці 28 днів, та 29 у високосному році.")
}